#: ~~~~~~ NOTICE: The primary purpose of this file is to help your translations
#: be consistent across episodes, and as a guide when multiple translators work
#: on the same language. secondary for generating transcripts. Comments
#: starting by #: are coming from _catalog.pot file, will always overwritten if
#: you edit them. To write own comments, please start them simply with # (no
#: colon) they'll stay around. You can read more about PO reference files in
#: translation documentation repository.
msgid ""
msgstr ""
"Language: Polish\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Translator: Name <email@adress.com>\n"

#: ~~~~~~ TRANSCRIPT SPECIFICS: Title of the episode
msgid "Title"
msgstr ""

#: Used for the sound effects ('SFX', onomatopoeia)
msgid "Sound"
msgstr ""

#: For anything written on a wall, sign, paper, etc.
msgid "Writing"
msgstr ""

#: For speech bubbles of the narrator
msgid "Narrator"
msgstr ""

#: For notes appearing under panels
msgid "Note"
msgstr ""

#: For the credits at end of each episode.
msgid "Credits"
msgstr ""

#: ~~~~~~ CHARACTERS (Episode 1)
msgid "Pepper"
msgstr "Pepper"

#: How the 'Pepper' & 'Carrot' words are connected in title -- for reference
#: only, not used by transcripts.
msgid "&"
msgstr ""

msgid "Carrot"
msgstr "Carrot"

#: (Episode 3, Episode 37) A male vendor at the market.
msgid "Vendor"
msgstr ""

#: A red-haired witch wearing luxurious jewels.
msgid "(Miss) Saffron"
msgstr "(Panienka) Saffron"

#: Saffron's pet, a white female cat.
msgid "Truffel"
msgstr "Truffel"

#: (Episode 6) A witch with curly black hair who is a princess and later queen.
msgid "Coriander"
msgstr "Coriander"

#: Coriander's pet, a male black cockerel.
msgid "Mango"
msgstr "Mango"

#: A witch: blonde, with pink- or red-irised eyes, in a kimono.
msgid "Shichimi"
msgstr "Shichimi"

#: Shichimi's pet, a male fox with multiple tails.
msgid "Yuzu"
msgstr "Yuzu"

#: Also known as Mayor Bramble, he is a man with 'van Dyke' beard, brown suit
#: and hat; presents the potion magic contests.
msgid "Mayor of Komona"
msgstr ""

#: Generic term for audiences.
msgid "Audience"
msgstr ""

#: Generic term for any bird.
msgid "Bird"
msgstr ""

#: (Episode 7) Generic term for any fairy.
msgid "Fairies"
msgstr ""

#: (Episode 8) Generic term for any monster.
msgid "Monster"
msgstr ""

#: (Episode 11) The bon-vivant or happy-go-lucky witch who is one of Pepper's
#: three godmothers.
msgid "Cumin"
msgstr "Cumin"

#: The tall, thin, and behaviourally-rigid witch who is one of Pepper's three
#: godmothers.
msgid "Cayenne"
msgstr "Cayenne"

#: The ancient, small, and wise leader witch who is one of Pepper's three
#: godmothers.
msgid "Thyme"
msgstr "Thyme"

#: Name of the young blond haired prince (as-yet-uncrowned king) Acren.
msgid "(Prince) Acren"
msgstr ""

#: Generic term used by Pepper to refer the three witches who raised her.
msgid "godmothers"
msgstr "babcie"

#: (Episode 18) Master and teacher of Hippiah, an elf with blond curly hair.
msgid "Basilic"
msgstr ""

#: A student witch of Hippiah, with long dark hair.
msgid "Oregano"
msgstr "Oregano"

#: A student witch of Hippiah, with short blond hair and freckles.
msgid "Cardamom"
msgstr "Cardamon"

#: A student witch of Hippiah and also an elf, with red hair.
msgid "Cinnamon"
msgstr "Cinnamon"

#: The main recurrent witch of Hippiah, a human with raccoon ears and tail.
msgid "Camomile"
msgstr "Camomile"

#: (Episode 21) A witch of Aquah, living in water, with a long white hair
#: crest.
msgid "Spirulina"
msgstr "Spirulina"

#: Spirulina's pet, a male _Betta splendens_ fish.
msgid "Durian"
msgstr "Durian"

#: (Episode 22) One of the jury members, he wears a white beard, monocle, and
#: wide-brimmed hat. His name references Frieza from Dragon Ball.
msgid "Lord Azeirf"
msgstr "Lord Azeirf"

#: One of the jury members, she is a queen. Her name and hairstyle reference
#: Leia Organa from Star Wars.
msgid "Queen Aiel"
msgstr "Królowa Aiel"

#: (Episode 23) The big and muscular golden walrus genie.
msgid "Genie of Success"
msgstr ""

#: (Episode 27) The royal tailor of Coriander.
msgid "Tailor"
msgstr ""

#: The robotic invention of Coriander.
msgid "Psychologist-Bot"
msgstr ""

#: (Episode 28) Generic term for any/all of the journalists asking questions at
#: coronation party.
msgid "Journalist"
msgstr ""

#: (Episode 32) A bearded king wearing golden armour.
msgid "King"
msgstr ""

#: The officer of the bearded king, with a long blond mustache.
msgid "Officer"
msgstr ""

#: Generic label used for all sounds produced by the armies.
msgid "Army"
msgstr ""

#: (Episode 33) The name of the king with a hunting horn, leading opposing
#: ('dark') army
msgid "Enemy"
msgstr ""

#: (Episode 34) A witch and knight of Ah, an elf from a desert. She is also the
#: teacher Shichimi.
msgid "Hibiscus"
msgstr ""

#: The supreme leader witch of Ah, with green hair.
msgid "Wasabi"
msgstr ""

#: (Episode 35) A witch of Ah who pilots a dragon, with spiky haircut and
#: aviator goggles. She is Shichimi's girlfriend.
msgid "Torreya"
msgstr ""

#: Torreya's pet, a white dragon.
msgid "Arra"
msgstr ""

#: (Episode 36) A rat appearing in the prison.
msgid "Rat"
msgstr ""

#: Generic terms for guards.
msgid "Guard"
msgstr ""

#: (Episode 37) The giant Phoenix
msgid "Phoenix"
msgstr ""

#: ~~~~~~ PLACES: (Episode 3) The flying city with the giant tree of Komona at
#: centre.
msgid "Komona City"
msgstr "Miasto Komona"

#: (Episode 6) The name of the village near Pepper's house.
msgid "Squirrel's End"
msgstr "Wiewiórczy Koniec"

#: A region of the world (Hereva) that federates many cities including ruled by
#: Coriander, Qualicity.
msgid "Technologist's Union"
msgstr "Unia Technologów"

#: A large region of the world (Hereva) also known as land Ah, Shichimi's land.
msgid "the lands of the setting moons"
msgstr "kraina zachodzących Księżyców"

#: (Episode 16) The name of the planet and general setting Pepper&Carrot.
msgid "Hereva"
msgstr "Hereva"

#: (Episode 27) Coriander's city, a large industrial city standing alone in the
#: middle of desert on rocky bluff.
msgid "Qualicity"
msgstr "Qualicity"

#: (Episode 31) A hill or small mountain sacred to the school of Chaosah.
msgid "Tenebrume"
msgstr ""

#: ~~~~~~ MAGIC: (Episode 8) The magic of chaos; that which Pepper now
#: practices.
msgid "Chaosah"
msgstr "Chaosah"

#: (Episode 18) The magic of plants and living creatures; that which Camomile
#: practices Pepper used to.
msgid "Hippiah"
msgstr "Hippiah"

#: (Episode 21) The magic of giving life to dead things including machines;
#: that which Coriander practices.
msgid "Zombiah"
msgstr "Zombiah"

#: The magic of fire, melting metals and cooking; that which Saffron practices.
msgid "Magmah"
msgstr "Magmah"

#: The magic of ghost and spirits; that which Shichimi practices.
msgid "Ah"
msgstr "Ah"

#: The magic of water, rain, and oceans; that which Spirulina practices.
msgid "Aquah"
msgstr "Aquah"

#: (Episode 24) The substance or unit of magic, the name which is derived from
#: "reality".
msgid "Rea"
msgstr ""

#: ~~~~~~ TIME SYSTEM: Weekday 1 − A day of recreation or rest, similar to
#: Sunday; associated with Chaosah, derived from the French word 'hazard'.
msgid "Azarday"
msgstr "Dzień Azara"

#: Weekday 2 - associated with Magmah, from babka, a sweet brioche cake.
msgid "Babkaday"
msgstr ""

#: Weekday 3 - associated with Aquah, from Ceto, a Greek mythological goddess
#: and sea monster.
msgid "Cetoday"
msgstr ""

#: Weekday 4 - associated with Zombiah, from Donn, the lord of dead in Irish
#: myth.
msgid "Donday"
msgstr ""

#: Weekday 5 - associated with Hippiah, egg = life.
msgid "Eggday"
msgstr ""

#: Weekday 6 - associated with Ah, from the Japanese mythological god
#: Fukurokuju, fuku meaning "happiness".
msgid "Fookuday"
msgstr ""

#: Weekday 7 - associated with all of the schools Hereva.
msgid "Zero's Day"
msgstr ""

#: Time - PM (afternoon)
msgid "Pinkmoon"
msgstr "Różoksiężyc"

#: Time - AM (morning)
msgid "Airmoon"
msgstr ""

#: ~~~~~~ MISC: (Episode 3) A vegetable at the market, a sort of squash shaped
#: like yellow star. Used as an ingredient in potions.
msgid "pumpkinstar"
msgstr "dyniogwiazdka"

#: The monetary unit of Komona city (and pun off 'kilo-octets', another word
#: for kilobytes).
msgid "Ko"
msgstr "Ko"

#: A potion ingredient, obtained from clouds.
msgid "pearls of mist"
msgstr "mgłowe perły"

#: A large, docile creature: half-dragon, half-cow.
msgid "DragonCow"
msgstr "Smocza Krowa"

#: (Episode 14) A potion ingredient, a plant.
msgid "Dragon's Tooth"
msgstr "smoczy ząb"

#: Name for a kind of dragon, blue and flying at high altitude.
msgid "Air Dragon"
msgstr "powietrzny smok"

#: Name for a kind of dragon, covered with mud from the swamps.
msgid "Swamp Dragon"
msgstr "bagnisty smok"

#: Name for a kind of dragon with body made electricity.
msgid "Lighting Dragon"
msgstr "gromowładny smok"

#: (Episode 21) Name of the newspaper published in Komona city and distributed
#: across Hereva.
msgid "The Komonan"
msgstr "Komonan"

#: (Episode 24) Name for the moonlit meeting of Chaosah big decisions and
#: exams.
msgid "council of The Three Moons"
msgstr "rada Trzech Księżyców"

#: (Episode 26) Name for a small sacred tree in the bottom of an abandoned
#: castle, generating continuous stream water.
msgid "Water-Tree"
msgstr "Wododrzew"
