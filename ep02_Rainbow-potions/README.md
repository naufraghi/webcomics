﻿# Episode 2: Rainbow Potions

![cover of episode 2](lang/gfx_Pepper-and-Carrot_by-David-Revoy_E02.png)

2016-01-08: To compile this episode before the 2016 refactoring, you'll need older Krita sources files in 2016-01-08_ep02_Potion-of-Flight.zip (available in /src folder of the episode [https://www.peppercarrot.com/0_sources/](https://www.peppercarrot.com/0_sources/)).
